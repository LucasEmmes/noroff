#include <iostream>
#include <fstream>
#include <vector>
using namespace std;

int main(int argc, char const *argv[])
{
    vector<string> lines;
    ifstream datafile;
    string filepath = "data.txt";

    datafile.open(filepath);

    if (datafile.is_open()) {
        string line;
        while (getline(datafile, line)) {
            cout << line << endl;
            lines.push_back(line);
        }
        datafile.close();
    }

    ofstream datafile2;
    datafile2.open("reverse.txt", ios::app);

    for (int i = lines.size()-1; i >= 0; i--) {
        datafile2 << lines[i] << endl;
    }
    datafile2.close();

    return 0;
}
