#include <iostream>
#include "user.h"
#include "tweet.h"
#include "controller.h"

int main(int argc, char const *argv[])
{
    Controller c;

    c.register_user("Lucas", "pwd");
    c.register_user("Eivind", "pwd");
    c.register_user("Alexander", "pwd");

    if (c.login("Lucas", "pwd")) {
        c.follow("Eivind");
        c.follow("Alexander");
        c.logout();
    }

    if (c.login("Eivind", "pwd")) {
        int i = c.make_tweet("Hello World");
        c.logout();
    }


    if (c.login("Lucas", "pwd")) {
        c.get_feed(1);
    }
    return 0;
}
