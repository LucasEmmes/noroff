#include "tweet.h"
#include "user.h"
#include <iostream>

Tweet::Tweet(User* author, std::string content, int reply_to) {
    this-> tweet_id = ++next_tweet_id;
    this->author = author;
    this->content = content;
    this->reply_to = reply_to;
};
        
int Tweet::get_tweet_id() {return tweet_id;}
User* Tweet::get_author() {return author;}
std::string Tweet::get_content() {return content;}

int Tweet::get_reply_to() {return reply_to;}
std::vector<User*> Tweet::get_likes() {return likes;}
std::vector<User*> Tweet::get_retweets() {return retweets;}
std::vector<Tweet*> Tweet::get_replies() {return replies;}

void Tweet::print() {
    std::cout
    << "@" << author->get_username() << " | ID: " << tweet_id << "\n"
    << content << std::endl;
}