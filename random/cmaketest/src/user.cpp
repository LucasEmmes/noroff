#include "user.h"
#include "tweet.h"

User::User(std::string username, std::string password) {
    this->user_id = ++next_user_id;
    this->username = username;
    this->password = password;
};

int User::get_user_id() {return user_id;}
std::string User::get_username() {return username;}
std::vector<int> User::get_likes() {return likes;}
std::vector<int> User::get_feed() {return feed;}
std::vector<std::string> User::get_followers() {return followers;}

void User::push_feed(int tweet_id) {
    feed.push_back(tweet_id);
}

bool User::check_password(std::string password) {
    return (this->password == password);
}

void User::new_follower(std::string username) {
    followers.push_back(username);
}