#pragma once
#include <string>
#include <vector>
class Tweet;

class User {
    private:
        inline static int next_user_id = 0;
        int user_id;
        std::string username;
        std::string password;
        std::vector<int> likes; 
        std::vector<std::string> followers;
        std::vector<int> feed; 

    public:
        User(std::string username, std::string password);
    
        int get_user_id();
        std::string get_username();
        std::vector<int> get_likes();
        std::vector<std::string> get_followers();

        bool check_password(std::string password);
        void push_feed(int tweet_id);
        std::vector<int> get_feed();
        void new_follower(std::string username);
};