#pragma once
#include <string>
#include <map>
#include <memory>
class Tweet;
class User;

class Controller {
    private:
        User* current_user = NULL;
        std::map<int, std::unique_ptr<Tweet>> tweets;
        std::map<std::string, std::unique_ptr<User>> users;


    public:
        bool register_user(std::string username, std::string password);
        bool login(std::string username, std::string password);
        void logout();

        int make_tweet(std::string content, int reply_to = -1);
        bool get_feed(int number_of_tweets);
        bool follow(std::string username);
        bool unfollow(std::string username);
        User* get_user(std::string username);
        Tweet* get_tweet(int tweet_id);
};