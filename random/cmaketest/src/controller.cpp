#include "controller.h"
#include "user.h"
#include "tweet.h"
#include <map>
#include <memory>

User* Controller::get_user(std::string username) {return users[username].get();}
Tweet* Controller::get_tweet(int tweet_id) {return tweets[tweet_id].get();}

bool Controller::register_user(std::string username, std::string password) {
    User* u = get_user(username);
    if (u == nullptr) {
        users[username] = std::make_unique<User>(username, password);
        return true;
    }
    return false;
}

bool Controller::login(std::string username, std::string password) {
    User* user = get_user(username);
    if (user != nullptr) {
        if (user->check_password(password)) {
            current_user = user;
            return true;
        }
    }
    return false;
}

void Controller::logout() {
    if (current_user != nullptr) {
        current_user = nullptr;
    }
}

int Controller::make_tweet(std::string content, int reply_to) {
    if (current_user) {
        std::unique_ptr<Tweet> tweet = std::make_unique<Tweet>(current_user, content, reply_to);
        int id = tweet.get()->get_tweet_id();
        tweets[id] = std::move(tweet);
        for (std::string username: current_user->get_followers()) {
            get_user(username)->push_feed(id);
        }
        return id;
    }
    return -1;
}

bool Controller::follow(std::string username) {
    if (current_user) {
        User* user = get_user(username);
        if (user) {
            user->new_follower(current_user->get_username());
            return true;
        }
    }
    return false;
}

bool Controller::get_feed(int number_of_tweets) {
    if (current_user) {
        std::vector<int> feed = current_user->get_feed();
        int start = feed.size()-1;
        int stop = feed.size()-1-number_of_tweets;
        for (int i = start; i > stop; i--) {
            get_tweet(feed[i])->print();
            return true;
        }
    }
    return false;
}