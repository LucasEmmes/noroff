#pragma once
#include <string>
#include <vector>
class User;

class Tweet {
    private:
        inline static int next_tweet_id = 0;
        int tweet_id;
        User* author;
        std::string content;

        int reply_to;
        std::vector<User*> likes;
        std::vector<User*> retweets;
        std::vector<Tweet*> replies;

    public:
        Tweet(User* author, std::string content, int reply_to);
        
        int get_tweet_id();
        User* get_author();
        std::string get_content();

        int get_reply_to();
        std::vector<User*> get_likes();
        std::vector<User*> get_retweets();
        std::vector<Tweet*> get_replies();

        void print();
};