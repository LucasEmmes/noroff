 ## Noroff
  
This repo will be for me to store all the work I do during the  
Noroff C++ course: assignments, katas, and other stuff.  
  
├── assignments  
│   └── month  
│       └── date.cpp  
├── kata  
│   └── month  
│       └── date.cpp  
└── README.md  
