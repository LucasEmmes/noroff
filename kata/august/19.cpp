#include <iostream>

inline bool check_validity(int* sudoku) {
    int sum = 0;
    int temp;
    for (int i = 0; i < 9; i++) {
        temp = sudoku[i];                   // Extract number in single for loop
        temp *= (temp < 10 && temp > 0);    // Make sure it is in range [1,9]
        sum ^= (1 << temp);                 // XOR 2**temp into sum
    }
    return (sum==1022);     // If sum == 1022, then every number from 1 to 9 appears exactly once
}

int main(int argc, char const *argv[]) {
    int sudoku[3][3] = {{1,2,3}, {4,5,6}, {7,8,9}};
    std::cout << check_validity(*sudoku) << std::endl;
    return 0;
}
