#include <iostream>
#include <string>
using namespace std;

int char_to_integer(char s) {
    return s - 48;
}

char integer_to_char(int n) {
    return '0' + n;
}

string add(string a, string b) {
    int temp = 0;
    string sum = "";

    // Get length of the longest string so we know how much to pad the other #branchless_but_unreadable
    int max_length = a.size()*(a.size() > b.size()) + b.size()*(a.size() <= b.size());
    // Pad with leading 0 to equal length
    while (a.size() < max_length) {
        a = "0" + a;
    }
    while (b.size() < max_length) {
        b = "0" + b;
    }

    // Add the strings together character by character starting from the back
    for (int i = max_length - 1; i >= 0; i--) {
        temp += char_to_integer(a[i]) + char_to_integer(b[i]);
        sum = integer_to_char(temp % 10) + sum;    // Only need add 1s position for now
        temp = temp / 10;                           // Divide by ten so the carry gets adjusted to 1s position
    }

    if (temp > 0) {
        sum = integer_to_char(temp) + sum;          // Add last carry in case we have one
    }

    return sum;

}

string fibonacci(int n) {
    string x = "0";
    string y = "1";
    string z = "0";
    for (int i = 1; i < n; i++) {
        z = add(x, y);
        x = y;
        y = z;
    }
    return z;
}

int main(int argc, char const *argv[]) {
    std::cout << fibonacci(std::stoi(argv[1])-1) << std::endl;
    return 0;
}