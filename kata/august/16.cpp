#include <iostream>

bool is_ugly(int n) {
    while(n%2==0) {n/=2;} // Remove 2 as a factor
    while(n%3==0) {n/=3;} // Remove 3 as a factor
    while(n%5==0) {n/=5;} // Remove 5 as a factor
    return (n==1); // If n==1, then only 2, 3, or 5 were factors
}

int main(int argc, char const *argv[]) {
    for (int i = 1; i <= 100; i++) {
        if (is_ugly(i)) {
            std::cout << i << std::endl;
        }
    }
    return 0;
}
