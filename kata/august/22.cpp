#include <iostream>
#include <vector>
#include <algorithm>

enum class Rule_type {
    NOT_INC, CP, BP
};

class Rule {
    public:
        std::vector<int> example;
        Rule_type type;   // NOT_INC
        int amount;
        Rule(std::vector<int> e, Rule_type t, int a) {
            example = e;
            type = t;
            amount = a;
        }
};

bool valid(Rule &r, std::vector<int> &guess) {
    int overlap = 0;
    switch (r.type)
    {
    case Rule_type::NOT_INC:
        for (int i = 0; i < 3; i++)
        {
            if (std::find(guess.begin(), guess.end(), r.example[i]) != guess.end()) {
                return false;
            }
        }
        return true;
        break;
    
    case Rule_type::CP:
        for (int i = 0; i < 3; i++)
        {
            if (r.example[i] == guess[i]) {
                overlap++;
            }
        }
        return overlap == r.amount;
        break;

    case Rule_type::BP:
        for (int i = 0; i < 3; i++)
        {
            if (std::find(guess.begin(), guess.end(), r.example[i]) != guess.end() && r.example[i] != guess[i]) {
                overlap++;
            }
        }
        return overlap == r.amount;
        break;

    default:
        break;
    }
    return false;
}

void print_key(std::vector<int> &guess) {
    for (int i = 0; i < 3; i++)
    {
        std::cout << guess[i];
    }
    std::cout << std::endl;
    
}

int main(int argc, char const *argv[])
{
    // Set up rules given
    std::vector<Rule> rules;
    rules.push_back(Rule({6,8,2}, Rule_type::CP, 1));
    rules.push_back(Rule({6,1,4}, Rule_type::BP, 1));
    rules.push_back(Rule({2,0,6}, Rule_type::BP, 2));
    rules.push_back(Rule({7,3,8}, Rule_type::NOT_INC, 0));
    rules.push_back(Rule({7,8,0}, Rule_type::BP, 1));

    // Set up guesses
    std::vector<std::vector<int>> brute;
    for (int i = 0; i < 1000; i++)
    {
        std::vector<int> guess;
        guess.push_back(i/100);
        guess.push_back((i%100)/10);
        guess.push_back((i%10));
        brute.push_back(guess);
    }
    
    // Start guessing
    bool pass = false;
    for (std::vector<int> guess : brute) {
        pass = true;
        for (Rule r: rules) {
            if (!valid(r, guess)) {
                pass = false;
            }
        }

        if (pass) {
            std::cout << "Found it!" << std::endl;
            print_key(guess);
        }
    }
    
    

    
    
    return 0;
}
