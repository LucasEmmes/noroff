#include <iostream>
#include <vector>

std::vector<std::vector<int>> vector_add(const std::vector<std::vector<int>>& m1, const std::vector<std::vector<int>>& m2) {

    if (m1.size() != m2.size()) {
        throw "Matrix dimensions do not match!";
    } else {
        for (int i = 0; i < m1.size(); i++)
        {
            if (m1[i].size() != m2[i].size() || m1[i].size() != m1[0].size())
            {
                throw "Matrix dimensions do not match!";
            }    
        }
    }

    std::vector<std::vector<int>> r(m1.size(), std::vector<int>(m1[0].size(), 0));
    
    for (int y = 0; y < m1.size(); y++)
    {
        for (int x = 0; x < m1[0].size(); x++)
        {
            r[y][x] = m1[y][x] + m2[y][x];
        }
        
    }
    
    return r;
}


std::vector<std::vector<int>> vector_multiply(const std::vector<std::vector<int>>& m1, const std::vector<std::vector<int>>& m2) {

    std::vector<std::vector<int>> r(m1.size(), std::vector<int>(m2[0].size(), 0));
    int sum;
    for (int y = 0; y < r.size(); y++)
    {
        for (int x = 0; x < r[0].size(); x++)
        {
            sum = 0;
            for (int i = 0; i < m2.size(); i++)
            {
                sum += m1[y][i]*m2[i][x];
            }
            r[y][x] = sum;
        }
    }
    
    return r;
}


int main(int argc, char const *argv[])
{
    std::vector<std::vector<int>> m1;
    std::vector<std::vector<int>> m2;

    m1 = {{1,2},{4,3}};
    m2 = {{1,-1,2},{0,-1,3}};

    for (std::vector<int>& row : vector_multiply(m1, m2)) {
        for (int& it : row)
            std::cout << it << std::endl;
    }


    return 0;
}
