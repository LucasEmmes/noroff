#include <iostream>
#include <string>
#include <cstdlib>
#include <fstream>

char shift(char letter, int amount, int direction) {
    // Squish
    letter = letter-(10*(letter==10)+31*(letter>=32 && letter<=126));
    // Shift
    letter = (letter + (amount*direction) + 95) % 95;
    // Unsquish
    letter = letter+(10*(letter==0)+31*(letter>0));
    return letter;
}

void crypt(std::string &message, int seed, int direction) {
    int shift_amount;
    srand(seed);
    for (int i = 0; i < message.size(); i++) {
        shift_amount = rand() % 95;
        message[i] = shift(message[i], shift_amount, direction);    
    }
}


int main(int argc, char const *argv[])
{
    std::string filepath = std::string(argv[1]);
    int key = std::stoi(argv[2]);
    int d = std::stoi(argv[3]);

    // std::string filepath = "input.txt";
    // int key = 10;
    // int d = 1;
    std::string message;

    
    {
        std::string temp;
        std::ifstream datafile;
        datafile.open(filepath);
        if (datafile.is_open()) {
            while (getline(datafile, temp)) {
                message += temp + "\n";
            };
            datafile.close();
        }
        message = message.substr(0, message.size()-1);
    }

    crypt(message, key, d);
    std::cout << message << std::endl;

    {
        std::ofstream datafile;
        datafile.open("output.txt");
        datafile << message << std::endl;
        datafile.close();
    }

    return 0;
}
