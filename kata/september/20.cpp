#include <iostream>
#include <vector>

int validate(const std::vector<int>& sequence) {
    if (sequence[0]*sequence[1]*sequence[2] == 4) {
        for (int i = 0; i < 3; i++)
        {
            if (!(sequence[i]-1)) return i;
        }
    }
    return -1;
}

int main(int argc, char const *argv[])
{
    std::string input = std::string(argv[1]);

    std::vector<std::vector<int>> board;
    for (int i = 0; i < 3; i++)
    {
        board.push_back(std::vector<int>());
        for (int j = 0; j < 3; j++)
        {
            switch (input[i*3+j])
            {
            case 'x':
                board[i].push_back(2);
                break;
            case 'o':
                board[i].push_back(0);
                break;        
            default:
                board[i].push_back(1);
                break;
            }
        }
    }
    
    std::vector<int> temp;

    for (int i = 0; i < 3; i++)
    {
        temp = {board[i][0], board[i][1], board[i][2]};
        int v = validate(temp);
        if (v >= 0) {
            std::cout << i << "," << v << std::endl;
            return 0;
        }
    }

    for (int i = 0; i < 3; i++)
    {
        temp = {board[0][i], board[1][i], board[2][i]};
        int v = validate(temp);
        if (v >= 0) {
            std::cout << v << "," << i << std::endl;
            return 0;
        }
    }
    
    int v;

    temp = {board[0][0], board[1][1], board[2][2]};
    v = validate(temp);
    if (v >= 0) {
        std::cout << v << "," << v << std::endl;
        return 0;
    }

    temp = {board[2][0], board[1][1], board[0][2]};
    v = validate(temp);
    if (v >= 0) {
        std::cout << v << "," << (2-v) << std::endl;
        return 0;
    }


    std::cout << "-1,-1" << std::endl;

    return 0;
}
