#include <iostream>
#include <cmath>

bool isprime(const std::string& number, bool left) {
    long long int n = std::stoll(number);	
    if (n==1) return false;
	int sqn = (int) std::sqrt((double) n);
	for (int i = 2; i <= sqn; i++) {
		if (n % i == 0) {
            return false;
		}
	}
    if (number.length() == 1) return true;
    else if (left) return isprime(number.substr(1, number.length()), true);
    else return isprime(number.substr(0, number.length()-1), false);
}

bool containsZero(const std::string& number) {
    for (const char& c: number)
    {
        if (c == '0')
            return true;
    }
    return false;
}

int main(int argc, char const *argv[])
{
    std::string number = std::string(argv[1]);
    if (argc == 1) return 0;
    if (containsZero(number)) {
        std::cout << "None" << std::endl;
        return 0;
    }
    
    bool left = isprime(number, true);
    bool right = isprime(number, false);
    if (left && right) std::cout << "Both" << std::endl;
    else if (left) std::cout << "Left" << std::endl;
    else if (right) std::cout << "Right" << std::endl;
    else std::cout << "None" << std::endl;

    return 0;
}
